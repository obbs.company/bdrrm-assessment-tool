<?php
  include('config.php');
  // Create connection
  $conn = new mysqli($servername, $username, $password, $database);

  if( !isset($_GET['brgy']) ){
    header('Location: register.php');
  } else {
    $brgy = $_GET['brgy'];
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Barangay Disaster Risk Reduction and Management Plan Template</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap.min.css">
  <link rel="stylesheet" href="editor/css/editor.bootstrap.min.css">
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
  <link rel="stylesheet" href="styles.css">
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
  <script src="editor/js/dataTables.editor.min.js"></script>
  <script src="editor/js/editor.bootstrap.min.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
    });
  </script>
</head>
<body class="bg-1">
  <div class="container">
    <div class="bg-2 jumbotron">
      <h1>Barangay Disaster Risk Reduction and Management Plan Template</h1>
      <p>Ito ang kabuuang impormasyon na dapat makita sa BDRRM Plan ng barangay.</p>
    </div>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">I. Profile</a></li>
      <li><a data-toggle="tab" href="#menu1">II. Structure</a></li>
      <li><a data-toggle="tab" href="#menu2">III. Assessment</a></li>
      <li><a data-toggle="tab" href="#menu3">IV. Legal</a></li>
      <li><a data-toggle="tab" href="#menu4">V. Summary</a></li>
      <li><a data-toggle="tab" href="#menu5">VI. Programs</a></li>
      <li><a data-toggle="tab" href="#menu6">VII. Monitoring</a></li>
      <li><a data-toggle="tab" href="#menu7">VIII. Annexes</a></li>
    </ul>

    <div class="tab-content">
      <div id="home" class="tab-pane fade in active">
        <h2>MGA PANGUNAHING IMPORMASYON TUNGKOL SA BARANGAY</h2>
        <p>Ang mga impormasyon na kakailanganin ay maaaring kopyahin sa Barangay Development Plan (BDP). Isulat ang impormasyon na hinhingi sa bawat blankong espasyo.</p>
        <h3>A. Anyong Pampisikal, Pangkapaligiran at Pangkalupaan ng Barangay (Geographical Classification)</h3>
          <?php 
            include('tab_1_a_1.html');
            include('tab_1_a_2.html');
            include('tab_1_a_3.html');
          ?>
        <h3>B. Mga Impormasyon tungkol sa Populasyon at Pinamamahayan</h3>
          <?php
            include('tab_1_b_1.html');
            include('tab_1_b_2.html');
            include('tab_1_b_3.html');
            include('tab_1_b_4.html');
            include('tab_1_b_5.html');            
          ?>
        <h3>C. Mga impormasyon tungkol sa Pangkabuhayan</h3>
          <?php    
            include('tab_1_c_1.html');
          ?>
        <h3>D. Mga Pangunahing Imprastraktura at Pasilidad sa Barangay</h3>
          <?php    
            include('tab_1_d_1.html');
            include('tab_1_d_2.html');
            include('tab_1_d_3.html');
            include('tab_1_d_4.html');
            include('tab_1_d_5.html');
          ?>
        <h3>E. Mga Pangunahing Pasilidad at Serbisyo sa Barangay</h3>
          <?php
            include('tab_1_e_1.html');
            include('tab_1_e_2.html');
            include('tab_1_e_3.html');
            include('tab_1_e_4.html');
            include('tab_1_e_5.html');
            include('tab_1_e_6.html');
          ?>
        <h3>F. Imbentaryo ng mga Institusyon, Sektor at iba pang mga Boluntaryong Grupo na nasa loob ng Barangay 
(Kabilang ang mga may kinalaman sa kapaligiran/kalikasan, kalusugan, at iba pa.)</h3>
          <?php
            include('tab_1_f_1.html');
          ?>
        <h3>G. Imbentaryo ng Lakas Paggawa (Human Resources)</h3>
          <?php
            include('tab_1_g_1.html');
            include('tab_1_g_2.html');
            include('tab_1_g_3.html');
          ?>
      </div>
      <div id="menu1" class="tab-pane fade">
        <h2>BDRRMC ORGANIZATIONAL STRUCTURE</h2>
          <?php 
            include('tab_2_a.html'); 
          ?>
      </div>
      <div id="menu2" class="tab-pane fade">
        <h2>PARTICIPATORY COMMUNITY RISK ASSESSMENT (CRA)</h2>
        <p>Ang Participatory Community Risk Assessment (CRA) ay isang paraan upang matukoy ang mga panganib o peligrong maaaring maranasan, at malaman ang kalakasan at kalawakang maaaring idulot ng panganib o peligro sa komunidad. Sa pamamagitan ito ng sama-samang pag-alam ng mga kalakasan at oportunidad na mayroon sa kapaligiran ng barangay upang makatulong sa pagpapababa ng peligro o panganib.</p>
        <p>Ito ay sama-samang ginagawa ng iba’t-ibang sektor na mayroon sa komunidad kasama dito ang mga sektor ng bata o kabataan, may kapansanan, matatanda, kababaihan at iba pang bolnerableng grupo o sektor</p>
        <h3>A. Pagtukoy ng mga kalamidad o disaster sa nakalipas na mga taon at epektong dulot ng mga nito sa komunidad</h3>
          <?php 
            include('tab_3_a_1.html');           
          ?>
        <h3>B. Pagtukoy sa mga posibleng peligro o bantang panganib na maaring maranasan ng barangay:</h3>
          <?php 
            include('tab_3_b.html');
            include('tab_3_b_1.html');           
          ?>
        <h3>C. Bulnerabilidad o Kahinaan ng Barangay</h3>
          <?php 
            include('tab_3_c.html');
            include('tab_3_c_1.html');        
          ?>
        <h3>D. Kapasidad o Kalakasan ng Barangay</h3>
          <?php 
            include('tab_3_d.html');
          ?>
        <h3>E. Mapa ng Barangay</h3>
          <?php 
            include('tab_3_e.html');
          ?>
        <h3>F. Pag-establish ng database ng pagkakalantad (exposure database) na maaaring maapektuhan ng pangunahing peligro o bantang panganib</h3>
          <?php 
            include('tab_3_f_1.html');
            include('tab_3_f_2.html');
            include('tab_3_f_3.html');
            include('tab_3_f_4.html');
          ?>
        <h3>G. Bilang ng mga tao o pamilyang maaaring maapektuhan ng peligro o bantang panganib kada purok o sitio ayon sa tatlong kategorya</h3>
          <?php 
            include('tab_3_g_1.html');
            include('tab_3_g_2.html');
          ?>
        <h3>H. Mga pangunahing isyu o suliranin na kinakaharap ng mga bulnerableng grupo tuwing mayroong kalamidad o disaster na nangyari sa loob ng barangay, tulad ng mga bata at kabataan, mga kababaihan, mga buntis, mga nanay na nagpapasuso, mga may kapansanan, mga nakatatanda o senior citizen at mga katutubo</h3>
          <?php 
            include('tab_3_h.html');
          ?>
        <h3>I. Listahan ng mga itinalagang evacuation center at mga pansamantalang tuluyan o isolation facilities ng barangay at munisipyo/syudad (pag-aari ng gobyerno o pribado)</h3>
          <?php 
            include('tab_3_i.html');
          ?>
          <h3>J. Imbentaryo ng mga ligtas na evacuation centers o lugar na pupuntahan ng mga pamilyang maaaring maapektuhan ng peligro o panganib</h3>
          <?php 
            include('tab_3_j.html');
          ?>
        <h3>K. Mga lugar na maaaring paglikasan ng mga tao sa panahon ng peligro o panganib na paparating o maaaring mangyari sa barangay</h3>
          <?php 
            include('tab_3_k.html');
          ?>
        <h3>L. Mga lugar o istruktura na maaaaring paglikasan ng mga pinanggagalingan ng kanilang ikinabubuhay (livestock,fishing boats, etc.)</h3>
          <?php 
            include('tab_3_l.html');
          ?>
        <h3>M.  Imbentaryo ng mga naka-stock o naka-preposition na mga kagamitan o goods (Food and Non-Food Items)</h3>
          <?php 
            include('tab_3_m.html');
          ?>
        <h3>N. Mga itinalagang evacuation center/lugar kung saan ipamamahagi ang mga relief goods (food and non-food items)</h3>
          <?php 
            include('tab_3_n.html');
          ?>
        <h3>O. Proseso at pamamaraan ng pamamahagi ng relief goods o tulong para sa mga apektadong pamilya o indibidwal</h3>
          <?php 
            include('tab_3_o.html');
          ?>
        <h3>P. Imbentaryo ng mga natanggap o nakuhang pagsasanay ng mga miyembro ng BDRRMC</h3>
          <?php 
            include('tab_3_p.html');
          ?>
        <h3>Q. Imbentaryo ng mga kagamitan sa pagkilos sa panahon ng kalamidad o disaster</h3>
          <?php 
            include('tab_3_q.html');
          ?>
        <h3>R. Sistema ng Agarang Babala (Early Warning Sa Pamayanan o Barangay (Community-Based EWS) para sa Natural, Human Induced, Conflict and Health Hazards (Refer to Health Alert Notification System)</h3>
          <?php 
            include('tab_3_r.html');
          ?>
      </div>
      <div id="menu3" class="tab-pane fade">
        <h2>LEGAL NA BATAYAN NG BDRRM PLAN</h2>
        <?php 
            include('tab_4_a.html');
        ?>
      </div>
      <div id="menu4" class="tab-pane fade">
        <h2>KABUUAN NG PROGRAMA SA BDRRM (Batay sa detalyadong PPAs sa ibaba)</h2>
        <h3>A. Proseso ng pagtukoy ng mga programa, proyekto at gawain sa BDRRM </h3>
          <?php 
            include('tab_5_a.html');           
          ?>
        <h3>B. Pagbubuo (pagsusuma) ng mga resulta ng Participatory/Community Risk Assessment</h3>
          <?php 
            include('tab_5_b.html');           
          ?>
      </div>
      <div id="menu5" class="tab-pane fade">
        <h2>PROGRAMS, PROJECTS, AND ACTIVITIES (PPAs)</h2>
        <h3>●	Functional Area</h3>
          <?php 
            include('tab_6.html');           
          ?>
      </div>
      <div id="menu6" class="tab-pane fade">
        <h2>MONITORING AND EVALUATION (Pagsusubaybay at Pagsusuri)</h2>
        <h3>A. Pagsubaybay at Pagsusuri ng mga gawaing nakatala sa Barangay DRRM Plan</h3>
          <?php 
            include('tab_7_a.html');         
          ?>
          <h3>B. Talaan at Paggagamitan ng Pondo mula sa Local Disaster Risk Reduction and Management Fund</h3>
          <?php 
            include('tab_7_b.html');            
          ?>
      </div>
      <div id="menu7" class="tab-pane fade">
        <h2>ANNEXES NG BDRRM PLAN</h2>
        <?php 
            include('tab_8_a.html');
        ?>
      </div>
    </div>
  </div>
</body>
</html>
<?php $conn->close(); ?>