**INSTALLATION**

This installation guide assumes that a working web server with MySQL and PHP is already in place.

1. Clone the repository

```
git clone https://gitlab.com/obbs.company/bdrrm-assessment-tool.git
```

2. Create the database

```
# cd bdrrm-assessment-tool/
# mysql -u root -p
mysql> create database lga;
mysql> use lga;
mysql> source 20210920_lga.sql;
mysql> exit;
```

3. Modify config.php.

```
$servername = "localhost";
$username = "user";
$password = "password";
$database = "lga";
```

4. Modify editor/lib/config.php.

```
$sql_details = array(
	"type" => "Mysql",     // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "user",          // Database user name
	"pass" => "password",          // Database password
	"host" => "localhost", // Database host
	"port" => "",          // Database connection port (can be left empty for default)
	"db"   => "lga",          // Database name
	"dsn"  => "",          // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   // PHP PDO attributes array. See the PHP documentation for all options
);
```


5. Visit the site while adding _/bdrrm-assessment-tool_ to the URL.

**UPDATE**

1. Get the updated and/or new files.

```
# cd bdrrm-assessment-tool/
git pull
```

2. Update the database.

```
# cd bdrrm-assessment-tool/
# mysql -u root -p
mysql> drop database lga;
mysql> create database lga;
mysql> use lga;
mysql> source 20210920_lga.sql;
mysql> exit;
```
