<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="UTF-8">
        <link rel="apple-touch-icon" type="image/png" href="https://cpwebassets.codepen.io/assets/favicon/apple-touch-icon-5ae1a0698dcc2402e9712f7d01ed509a57814f994c660df9f7a952f3060705ee.png" />
        <meta name="apple-mobile-web-app-title" content="CodePen">
        <link rel="shortcut icon" type="image/x-icon" href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico" />
        <link rel="mask-icon" type="" href="https://cpwebassets.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg" color="#111" />
        <title>Register</title>
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
        <style>
            option {
                margin: 0.5em;
            }
        </style>
        <script>
            window.console = window.console || function(t) {};
        </script>
        <script>
            if (document.location.search.match(/type=embed/gi)) {
                window.parent.postMessage("resize", "*");
            }
        </script>
    </head>
    <body translate="no" >
        <?php
            $servername = "localhost";
            $username = "admin";
            $password = "R2N19LpJElOn";
            $dbname = "lga";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
            }
            $conn -> set_charset("utf8");
            //echo "Initial character set is: " . $conn -> character_set_name();
        ?>
        <div class="col-xs-4">
        </div>
        <div class="col-xs-4">
            <br>
            <?php
            if( isset($_GET['alert'] )){
                if( $_GET['alert']==0 ){
                    echo "<div class='alert alert-danger alert-dismissible'>";
                    echo "    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
                    echo "    <strong>Attention!</strong> Please fill-up all fields.";
                    echo "</div>";
                }
            }
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">Informant Registration</div>
                <div class="panel-body">
                    <form action=do_register.php>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" /> 
                        </div>
                        <div class="form-group">
                            <label for="city">City/Municipality, Province</label>
                            <select class="form-control" name="city" id="city">
                                <option value="0">Please select city/municipality</option>
                                <?php
                                    $sql = "SELECT DISTINCT city FROM brgy ORDER BY city ASC";
                                    $result = $conn->query($sql);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value='".$row['city']."'>".$row['city']."</option>";
                                        }
                                    } else {
                                        //echo "0 results";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="brgy">Barangay</label>
                            <select class="form-control" name="brgy" id="brgy">
                                <option value1="0">Please select barangay</option>
                                <?php
                                    $sql = "SELECT * FROM brgy";
                                    $result = $conn->query($sql);

                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {
                                            echo "<option value1='".$row['city']."' value='".$row['code']."'>".$row['brgy']."</option>";
                                        }
                                    } else {
                                        //echo "0 results";
                                    }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
        </div>
        <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-8216c69d01441f36c0ea791ae2d4469f0f8ff5326f00ae2d00e4bb7d20e24edb.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <script id="rendered-js" >
            //Reference: https://jsfiddle.net/fwv18zo1/
            var $city = $('#city'),
            $brgy = $('#brgy'),
            $options = $brgy.find('option');
            $city.on('change', function () {
            $brgy.html($options.filter('[value1="' + this.value + '"]'));
            }).trigger('change');
            //# sourceURL=pen.js
        </script>
        <?php
            $conn->close();
        ?>
    </body>
</html>
 
