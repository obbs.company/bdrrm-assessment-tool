<?php
  include('config.php');
  // Create connection
  $conn = new mysqli($servername, $username, $password, $database);

  if( !isset($_GET['id']) ){
    header('Location: register.php');
  } else {
    $id = $_GET['id'];
  }

  $sql = "SELECT * FROM tab_3_a_1 WHERE id=$id";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $code = $row['code'];
      $year = $row['var_1'];
      $name = $row['var_2'];
    }
  } else {
    echo "0 results";
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Barangay Disaster Risk Reduction and Management Plan Template</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap.min.css">
  <link rel="stylesheet" href="editor/css/editor.bootstrap.min.css">
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
  <link rel="stylesheet" href="styles.css">
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
  <script src="editor/js/dataTables.editor.min.js"></script>
  <script src="editor/js/editor.bootstrap.min.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
    });
  </script>
</head>
<body class="bg-1">
  <div class="container">
    <div class="bg-2 jumbotron">
      <h1>Barangay Disaster Risk Reduction and Management Plan Template</h1>
      <p>Ito ang kabuuang impormasyon na dapat makita sa BDRRM Plan ng barangay.</p>
    </div>
    <div>
      <h2><?php echo $name." (".$year.")"; ?></h2>
      <h4><?php echo "Epekto ng Kalamidad"; ?></h4>
    </div>
    <?php 
      include('tab_3_a_1_1.html'); 
      include('tab_3_a_1_2.html');
      include('tab_3_a_1_3.html');
      include('tab_3_a_1_4.html'); 
      include('tab_3_a_1_5.html'); 
      include('tab_3_a_1_6.html'); 
      include('tab_3_a_1_7.html'); 
    ?>
  </div>
</body>
</html>
<?php $conn->close(); ?>