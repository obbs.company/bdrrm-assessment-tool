<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "editor/lib/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'tab_3_a_1' )
	->where( 'code', $_POST['id'] )
	->fields(
		Field::inst( 'id' ),
		Field::inst( 'code' ),
		Field::inst( 'var_1' ),
		Field::inst( 'var_2' ),
		Field::inst( 'var_3' )
	)
	->process( $_POST )
	->json();
